﻿#include <iostream>
#include <cassert>

template<class T>
class Vector {
private:
	T *array;
	int size;
	int capacity;
	void extendCapacity();
public:
	Vector() :size(0), capacity(0), array(nullptr) {};
	~Vector();
	void Push(const T& elem);
	T& Pop();
	int GetSize();
	T& operator[](int index);
};








template<class T>
Vector<T>::~Vector() {
	delete[] array;
}

template<class T>
T& Vector<T>:: operator[](int index) {
	assert(index < size);
	return array[index];
}


template<class T>
int Vector<T>::GetSize() {
	return size;
}

template<class T>
T& Vector<T>::Pop() {
	return array[--size];
}

template<class T>
void Vector<T>::extendCapacity() {
	capacity = capacity == 0 ? 1 : capacity * 2;
	T* newArray = new T[capacity];
	for (int i = 0; i < size; i++) {
		newArray[i] = array[i];
	}
	delete[] array;
	array = newArray;
}

template<class T>
void Vector<T>::Push(const T& elem) {
	if (size == capacity) {
		extendCapacity();
	}
	array[size] = elem;
	size++;
}

template<class T,class Compare>
class BinHeap {
public:
	void Push(const T& elem);
	T& FindMin();
	T& ExtractMin();
	void ChangeValue(int lineNum, int num);

private:
	Vector<T> vector;
	Compare cmp;
	void shiftDown(int index);
	void shiftUp(int index);

};

template<class T, class Compare>
void BinHeap<T, Compare>::shiftDown(int index) {
	while (index >= 0) {
		int indexLeftchild = 2*index + 1;
		int indexRightchild = 2*index + 2;
		int indexMin = index;

		if (indexLeftchild < vector.GetSize() && cmp(vector[indexLeftchild], vector[indexMin]))
			indexMin = indexLeftchild;

		if (indexRightchild < vector.GetSize() && cmp(vector[indexRightchild], vector[indexMin]))
			indexMin = indexRightchild;
		if (indexMin != index) {
			std::swap(vector[index], vector[indexMin]);
			index = indexMin;
		}
		else {
			index = -1;
		}
	}
}

template<class T, class Compare>
void BinHeap< T, Compare>::shiftUp(int index) {

	while (index > 0) {
		int indexParent = (index - 1) / 2;
		if (cmp(vector[index], vector[indexParent])) {
			std::swap(vector[index], vector[indexParent]);
			index = indexParent;
		}
		else
			index = -1;
	}
}

template<class T, class Compare>
void BinHeap< T,  Compare>::Push(const T& elem) {
	vector.Push(elem);
	shiftUp(vector.GetSize() - 1);
}

template<class T, class Compare>
T& BinHeap< T,  Compare>::FindMin() {
	T val = vector[0];
	return val;
}
template<class T, class Compare>
T& BinHeap< T,  Compare>::ExtractMin() {
	T val = vector[0];
	vector[0] = vector.Pop();
	shiftDown(0);
	return val;
}


class ComparatorDeffault {
public:
	bool operator()(int first, int second) {
		return first < second;
	}
};



int main()
{
	BinHeap<int, ComparatorDeffault> heap;
	heap.Push(6);
	heap.Push(5);
	heap.Push(4);
	heap.Push(3);
	heap.Push(2);
	heap.Push(1);
	heap.Push(0);
	heap.Push(-1);
	for (int i = 0; i <= 6; i++) {
		std::cout << heap.ExtractMin() << "\n";
	}

}
